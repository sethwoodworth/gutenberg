"""Project Gutenberg corpus."""


from __future__ import absolute_import
from gutenberg.gutenberg import GutenbergCorpus  # noqa


__all__ = ['gutenberg']
